# mongodbapp
mongodbapp merupakan sebuah aplikasi microservice yang bertanggung jawab dalam mengolah data buku perpustakaan <br>
**Setup**<br>
Port menggunakan default dari FastAPI, yaitu port 8000 <br>
**Dokumentasi API**<br>
1. Mengambil data semua buku<br>
**GET** `localhost:8000/books` <br>
2. Mengambil data buku berdasarkan id <br>
**GET** `localhost:8000/bookbyid` <br>
    - Body <br>
        - raw JSON <br> 
            ```
            {
                "id":"_id bukunya_"
            }
            ```

3. Mengambil data buku berdasarkan nama <br>
**GET** `localhost:8000/bookbyname`<br>
    - Body<br>
        - raw JSON <br>
            ```
            {
                "nama":"_nama bukunya_"
            }   
            ```





